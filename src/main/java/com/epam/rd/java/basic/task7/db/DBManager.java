package com.epam.rd.java.basic.task7.db;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;
	private Connection connection;
	private static final String ERROR = "Transaction has been failed";

	public static synchronized DBManager getInstance() {
		try {
			if (instance == null) {
				instance = new DBManager();
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return instance;
	}

	private DBManager() {
		try {
			connection = DriverManager.getConnection(connect());
		} catch (SQLException e2) {
			e2.printStackTrace();
		}
	}

	private String connect() {
		Properties properties = new Properties();
		try (InputStream stream = Files.newInputStream(Paths.get("app.properties"))) {
			properties.load(stream);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return properties.getProperty("connection.url");
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		String sql = "SELECT * FROM users";
		try (Statement stmt = connection.createStatement();
			 ResultSet rs = stmt.executeQuery(sql)) {
			while (rs.next()) {
				User u = new User();
				u.setId(rs.getInt("id"));
				u.setLogin(rs.getString("login"));
				users.add(u);
			}
		} catch (SQLException e3) {
			// log
			e3.printStackTrace();
			throw new DBException(ERROR, e3);
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		String sql = "INSERT INTO users (login) VALUES (?)";
		try (PreparedStatement stmt = connection.prepareStatement(sql)) {
			stmt.setString(1, user.getLogin());
			stmt.execute();
			user.setId(getUser(user.getLogin()).getId());
			return true;
		} catch (SQLException e4) {
			// log
			e4.printStackTrace();
			throw new DBException(ERROR, e4);
		}
	}


	public boolean deleteUsers(User... users) throws DBException {
		String sql = "DELETE FROM users WHERE login = ?";
		try (PreparedStatement stmt = connection.prepareStatement(sql)) {
			for (User user : users) {
				stmt.setString(1, user.getLogin());
				stmt.executeUpdate();
			}
			return true;
		} catch (SQLException e5) {
			// log
			e5.printStackTrace();
			throw new DBException(ERROR, e5);
		}
	}

	public User getUser(String login) throws DBException {
		String sql = "SELECT id FROM users WHERE login = ?";
		try (PreparedStatement stmt = connection.prepareStatement(sql)) {
			stmt.setString(1, login);
			ResultSet rs = stmt.executeQuery();
			rs.next();
			User u = new User();
			u.setLogin(login);
			u.setId(rs.getInt("id"));
			return u;
		} catch (SQLException e6) {
			// log
			e6.printStackTrace();
			throw new DBException(ERROR, e6);
		}
	}

	public Team getTeam(String name) throws DBException {
		String sql = "SELECT id FROM teams WHERE name = ?";
		try (PreparedStatement stmt = connection.prepareStatement(sql)) {
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			rs.next();
			Team t = new Team();
			t.setName(name);
			t.setId(rs.getInt("id"));
			return t;
		} catch (SQLException e7) {
			// log
			e7.printStackTrace();
			throw new DBException(ERROR, e7);
		}
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		String sql = "SELECT * FROM teams";
		try (Statement stmt = connection.createStatement();
			 ResultSet rs = stmt.executeQuery(sql)) {
			while (rs.next()) {
				Team t = new Team();
				t.setId(rs.getInt("id"));
				t.setName(rs.getString("name"));
				teams.add(t);
			}
		} catch (SQLException e8) {
			// log
			e8.printStackTrace();
			throw new DBException(ERROR, e8);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		String sql = "INSERT INTO teams (name) VALUES (?)";
		try (PreparedStatement stmt = connection.prepareStatement(sql)) {
			stmt.setString(1, team.getName());
			stmt.executeUpdate();
			team.setId(getTeam(team.getName()).getId());
			return true;
		} catch (SQLException e9) {
			// log
			e9.printStackTrace();
			throw new DBException(ERROR, e9);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		String sql = "INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)";
		try (PreparedStatement stmt = connection.prepareStatement(sql)) {
			connection.setAutoCommit(false);
			for (Team team : teams) {
				stmt.setInt(1, user.getId());
				stmt.setInt(2, team.getId());
				stmt.executeUpdate();
			}
			connection.commit();
			connection.setAutoCommit(true);
			return true;
		} catch (SQLException e10) {
			try {
				connection.rollback();
			} catch (SQLException e11) {
				e11.printStackTrace();
				// log
				throw new DBException(ERROR, e11);
			}
		}
		return false;
	}


	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		String sql = "SELECT team_id FROM users_teams WHERE user_id = ?";
		try (PreparedStatement stmt = connection.prepareStatement(sql)) {
			stmt.setInt(1, user.getId());
			ResultSet rs = stmt.executeQuery();
			 while (rs.next()) {
				Team t = new Team();
				t.setId(rs.getInt("team_id"));
				setIdService(t);
				teams.add(t);
				}
		} catch (SQLException e13) {
			// log
			e13.printStackTrace();
			throw new DBException(ERROR, e13);
		}
		return teams;
	}

	public void setIdService(Team t) throws DBException {
		String sql1 = "SELECT name FROM teams WHERE id = ?";
		try (PreparedStatement stmt = connection.prepareStatement(sql1)) {
			stmt.setInt(1, t.getId());
			ResultSet rs1 = stmt.executeQuery();
			rs1.next();
			t.setName(rs1.getString("name"));
		} catch (SQLException e12) {
			// log
			e12.printStackTrace();
			throw new DBException(ERROR, e12);
		}
	}


	public boolean deleteTeam(Team team) throws DBException {
		String sql = "DELETE FROM teams WHERE name = ?";
		try (PreparedStatement stmt = connection.prepareStatement(sql)) {
			 stmt.setString(1, team.getName());
			 stmt.executeUpdate();
			return true;
		} catch (SQLException e14) {
			// log
			e14.printStackTrace();
			throw new DBException(ERROR, e14);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		String sql = "UPDATE teams SET name = ? WHERE id = ? ";
		try (PreparedStatement stmt = connection.prepareStatement(sql)) {
			stmt.setString(1, team.getName());
			stmt.setInt(2, team.getId());
			stmt.executeUpdate();
			return true;
		} catch (SQLException e15) {
			// log
			e15.printStackTrace();
			throw new DBException(ERROR, e15);
		}
	}
}
